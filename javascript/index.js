/*
CSU VM Naming Convention

API1PR01ASRWS1a.csu.edu <-- I like to use this server name for testing my changes.
System:	3-char alpha	API
Pipeline #:	1-char numeric	1
Env Type:	2-char alpha	PR
Env Type Seq #	2-char, left-padded numeric	01
Application:	3-char alpha	ASR
Application Machine Role:	2-char alpha	WS
Seq#/Cluster #	1-char numeric	1 --This should be a monotonically increasing number starting at 01 [0-9][0-9]
Cluster member Indicator	optional, 1-char alpha	a [a-z]
*/

//Call API to retrieve data.
function validateServerName(){
  var ourRequest = new XMLHttpRequest();
  //ourRequest.open('GET', 'http://52.24.180.139:8080/vm-naming-convention'); 
  ourRequest.open('GET', 'http://localhost:8080/vm-naming-convention');
  ourRequest.onload = function(){
    var ourData = JSON.parse(ourRequest.responseText);
    validation(ourData)
  }
  ourRequest.send();
}

//Validate server name.
function validation(data){
  var resultData = {};
  var servernameIsValid = true;

  //clear existing error messages. and hide results table.
  var messages = document.getElementsByClassName("message");
  for(var i = 0; i < messages.length; i++){
     document.getElementsByClassName("message")[i].innerHTML = "";
  }
  hideTable();

  //remove domain (.csu.edu) from serverName if exists.
  var serverName = document.getElementById('txtServerName').value;
  if(serverName.endsWith(".csu.edu")){
    serverName = serverName.slice(0,-8);
  }

  //Validate serverName length (at least 14 characters)
  if (serverName.length < 14){
    var message = "Server name is too short. Server names are at least 14 characters.";
    document.getElementById("validationErrorMessage").innerHTML = message;
    servernameIsValid = false;

  }

  //Validate System code (3-char alpha) *make this one look like the pipeline step.
  var systemCode = serverName.substring(0,3);
  if (!systemCode.match(/[a-z][a-z][a-z]/i)){
    var message = "System code is not valid. System codes strings of 3-letters."
    document.getElementById("systemCodeErrorMessage").innerHTML = message;
    servernameIsValid = false;
  }
  else {
    var isKnownSystemCode = false;
    for(i = 0 ; i < data.systemcode.length ; i++){
        var knownSystemCode = data.systemcode[i].code;
        if (systemCode == knownSystemCode){
          isKnownSystemCode = true;
          resultData.systemcodeDescription = data.systemcode[i].description;
        }
    }
    if(!isKnownSystemCode){
      var message = "The system code you have entered is not known.";
      document.getElementById("systemCodeErrorMessage").innerHTML = message;
      servernameIsValid = false;
    }
  }

  //Validate Pipeline
  var pipeline = serverName.substring(3,4);
  var isKnownPipeline = false;
  for(i = 0 ; i < data.pipeline.length ; i++){
    var knownPipeline = data.pipeline[i].code;
    if (pipeline == knownPipeline){
      isKnownPipeline = true;
      resultData.pipelineDescription = data.pipeline[i].description;
    }
  }
  if(!isKnownPipeline){
    var message = "The pipeline you have entered is not known.";
    document.getElementById("pipelineErrorMessage").innerHTML = message;
    servernameIsValid = false;
  }

  //Validate Environment Type
  var environmenttype = serverName.substring(4,6);
  var isKnownEnvironmentType = false;
  for(i = 0 ; i < data.environmenttype.length ; i++){
    var knownEnvironmentType = data.environmenttype[i].code;
    if(environmenttype == knownEnvironmentType){
      isKnownEnvironmentType = true;
      resultData.environmentTypeDescription = data.environmenttype[i].description;
    }
  }
  if(!isKnownEnvironmentType){
    var message = "The environment type you have entered is not known.";
    document.getElementById("environmenttypeErrorMessage").innerHTML = message;
    servernameIsValid = false;
  }

  //Validate Environment Type Sequence number [0-9][0-9]
  var environmenttypeSequenceNumber = serverName.substring(6,8);
  var isValidEnvironmentTypeSequenceNumber = false;
  if (environmenttypeSequenceNumber.match(/[0-9][0-9]/i)){
    isValidEnvironmentTypeSequenceNumber = true;
    resultData.environmentTypeSequenceNumberDescription = environmenttypeSequenceNumber;
  }
  if(!isValidEnvironmentTypeSequenceNumber){
    var message = "Environment type sequence number is not valid. Sequence numbers are two numeric characters."
    document.getElementById("systemCodeErrorMessage").innerHTML = message;
    servernameIsValid = false;
  }


  //Validate Application and Application Machine Role.
  var application = serverName.substring(8,11);
  var isKnownApplication = false;
  for(i = 0 ; i < data.application.length ; i++){
    var knownApplication = data.application[i].applicationcode;
    if(application == knownApplication){
      isKnownApplication = true;
      resultData.applicationDescription = data.application[i].description;

      //Validate application machine role.
      var applicationrole = serverName.substring(11,13);
      var isKnownApplicationRole = false
      for(j = 0 ; j < data.application[i].applicationroles.length ; j++){
        var knownApplicationRole = data.application[i].applicationroles[j].applicationrole;
        if(applicationrole == knownApplicationRole){
          isKnownApplicationRole = true;
          resultData.applicationRoleDescription = data.application[i].applicationroles[j].description;
        }
      }
      if(!isKnownApplicationRole){
        var message = "The application role you have entered is not known.";
        document.getElementById("applicationRoleErrorMessage").innerHTML = message;
        servernameIsValid = false;
      }
    }
  }
  if(!isKnownApplication){
    var message = "The application you have entered is not known.";
    document.getElementById("applicationErrorMessage").innerHTML = message;
    servernameIsValid = false;
  }

  //Valid sequence/cluster #
  var sequence_clusterNumber = serverName.substring(13,14);
  var isValidSequenceOrClusterNumber = false;
  if (sequence_clusterNumber.match(/[0-9]/i)){
    isValidSequenceOrClusterNumber = true;
    resultData.sequence_clusterNumberDescription = sequence_clusterNumber;
  }
  if(!isValidSequenceOrClusterNumber){
    var message = 'Sequence/cluster number is not valid. The Sequence/cluster number is a single numeric character.';
    document.getElementById('sequence_clusterNumberErrorMessage').innerHTML = message;
    servernameIsValid = false;
  }


  //Validate optional cluster member indicator *one alpha character
  var clusterMemberIndicator = serverName.substring(14 , 15);
  var isValidClusterMemberIndicator = false;
  if (clusterMemberIndicator.match(/[a-z]/i)){
    isValidClusterMemberIndicator = true;
    resultData.clusterMemberIndicatorDescription = clusterMemberIndicator;
  }
  if(!isValidClusterMemberIndicator){
    var message = 'Cluster member indicator is not valid. Cluster member indicator is a single letter'
    document.getElementById("clusterMemberIndicatorErrorMessage").innerHTML = message;
    servernameIsValid = false;
  }

  //Show results for valid server name.
  if (servernameIsValid){
    unhideTable();
    document.getElementById("systemDescription").innerHTML = resultData.systemcodeDescription;
    document.getElementById("pipelineDescription").innerHTML = resultData.pipelineDescription;
    document.getElementById("environmentTypeDescription").innerHTML = resultData.environmentTypeDescription;
    document.getElementById("environmentTypeSequenceNumberDescription").innerHTML = resultData.environmentTypeSequenceNumberDescription;
    document.getElementById("applicationDescription").innerHTML = resultData.applicationDescription;
    document.getElementById("applicationRoleDescription").innerHTML = resultData.applicationRoleDescription;
    document.getElementById("sequence_clusterNumberDescription").innerHTML = resultData.sequence_clusterNumberDescription;
    document.getElementById("clusterMemberIndicatorDescription").innerHTML = resultData.clusterMemberIndicatorDescription;
  }
}


//hide table
function hideTable(){
  document.getElementById('tblResultTable').style.visibility = 'hidden';
}

//unhide table
function unhideTable(){
  document.getElementById("tblResultTable").style.visibility = "visible";
}

//submit form on 'enter' keydown event.
window.addEventListener("keydown", checkKeyPressed);
function checkKeyPressed(keyPressed) {
    if (keyPressed.keyCode == "13") {
        validateServerName();
        keyPressed.preventDefault();
    }
}
