#!/bin/bash

#build image
docker build -t decipher-tool-frontend:local .
echo 'container built'

#remove existing container with the same name
docker container stop decipher-tool-frontend
echo 'container stopped'

docker container rm decipher-tool-frontend
echo 'container removed'

#run new container
docker run -d --name decipher-tool-frontend -p 3001:80 decipher-tool-frontend:local
docker container ls