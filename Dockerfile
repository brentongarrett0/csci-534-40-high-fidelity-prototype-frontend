FROM centos:latest
LABEL application="Server Name Deciphering Tool - Frontend" maintainer="bgarrett@gci.com"
RUN yum -y install httpd
COPY . /var/www/html/
ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
EXPOSE 80